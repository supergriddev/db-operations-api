'use strict';


var mongoose = require('mongoose'),
    dbOperations = mongoose.model('devices');

exports.list_all_devices_within_organization = function(req, res) {
    dbOperations.find({}, function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};


exports.list_all_devices_within_project = function(req, res) {
    dbOperations.findById(req.params.projectId, function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};

exports.find_a_device_by_udid = function(req, res) {
    dbOperations.find(req.params, function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};

exports.update_a_device = function(req, res) {
    dbOperations.findOneAndUpdate(req.params, req.body, {new: true}, function(err, task) {
        if (err)
            res.send(err);
        res.json(task);
    });
};


exports.insert_a_device = function(req, res) {
    var newDevice = new dbOperations(req.body);
    newDevice.save(function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json(deviceTask);
    });
};


exports.delete_all_devices_within_organization = function(req, res) {
    dbOperations.remove({

    }, function(err, deviceTask) {
        if (err)
            res.send(err);
        res.json({ message: 'Devices were successfully deleted' });
    });
};




