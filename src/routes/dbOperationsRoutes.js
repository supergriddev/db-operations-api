'use strict';
module.exports = function(app) {
    var dbOperations = require('../controllers/dbOperationsController');

    app.route('/dbOperations/findAllDevices')
        .get(dbOperations.list_all_devices_within_organization);

    app.route('/dbOperations/deleteAllDevices')
        .delete(dbOperations.delete_all_devices_within_organization);

    app.route('/dbOperations/findDeviceByUdid/:udid')
        .get(dbOperations.find_a_device_by_udid);

    app.route('/dbOperations/updateDeviceByUdid/:udid')
        .put(dbOperations.update_a_device);

    app.route('/dbOperations/insertDeviceInfo')
        .post(dbOperations.insert_a_device);

};