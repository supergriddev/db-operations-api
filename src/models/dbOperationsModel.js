'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var dbOperationsSchema = new Schema({

    name: {
        type: String,
        required: 'Name of the Device'
    },
    type: {
        type: String,
        required: 'type of the Device'
    },
    udid: {
        type: String,
        required: 'UDID of the Device'
    },
    version: {
        type: String,
        required: 'Version of the Device'
    },
    sessionId: {
        type: String,
        required: 'Session Id of the Device'
    },
    status: {
        type: String,
        required: 'Status of the Device'
    },
    os: {
        type: String,
        required: 'Operating System of the Device'
    },
    manufacturer: {
        type: String,
        required: 'Manufacturer of the Device'
    },
    model: {
        type: String,
        required: 'Model of the Device'
    }
});

module.exports = mongoose.model('devices', dbOperationsSchema);
