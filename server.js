let mongoose = require('mongoose'),
    express = require('express'),
    app = express(),
    DeviceInfo = require('./src/models/dbOperationsModel'), //created model loading here
    bodyParser = require('body-parser'),
    config = require('../config'),
    setPort = config.defaultPort;


// mongoose instance connection url connection
mongoose.Promise = global.Promise;

let dbPort = `${config.getGlobalVariable('databasePort')}`;
let apiPort = `${config.getGlobalVariable('apiPort')}`;


mongoose.connect(`mongodb://localhost:${dbPort}/grid`);


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./src/routes/dbOperationsRoutes'); //importing route
routes(app); //register the route


app.listen(`${apiPort}`);